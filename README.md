# aws-terraform-framework

### requirements:
* only support gitlab CI/CD tool
* terraform >= 1.0 obviously it's terraform project
* tfenv to manages terraform version upgrades
### optional but high recommended for local troubleshooting:
   * zsh with autoenv for autoload gitlab variables ( no generic implementation ready) 
## bootstrap
This process will create all the base necessary backend to create a terraform CI/CD

* clone this gitlap repository with the project name of your choice.
* create a service account and set the env vars in the repo settings.
* remove the .git folder re initialize the repository pointing to your gitlab.
* Setup your AWS Credentials environments variables in your local.
* rename gitlab-ci.yml to .gitlab-ci.yml
* go to the bootstrap folder 
* exec bash ./bootstrap.sh this will create all backend you need in aws to start working with the pipeline
* push the Init commit.

## License
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
