aws_account_id       = 
region               = "us-east-1"
env                  = "dev"
domain               = "mydomain.tld"
tags = {
  env             = "dev"
  product         = "lend"
  country         = "cl"
  source_of_truth = "glacier-infra"
}
