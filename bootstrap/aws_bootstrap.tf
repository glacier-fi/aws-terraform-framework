resource "aws_s3_bucket" "terraform_state" {
  bucket = var.project_name
  # Enable versioning so we can see the full revision history of our
  # state files
  versioning {
    enabled = true
  }
  # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "${var.project_name}-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_iam_user" "terraform-ci" {
  name = "terraform-ci"
  path = "/devsecops/"
}

resource "aws_iam_access_key" "terraform-ci" {
  user = aws_iam_user.terraform-ci.name
}

resource "aws_iam_user_policy" "terraform-ci" {
  name = "terraform-ci"
  user = aws_iam_user.terraform-ci.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

output terraform-ci-id {
  value = aws_iam_access_key.terraform-ci.id
}
output "terraform-ci-key" {
  value = nonsensitive(aws_iam_access_key.terraform-ci.secret)
}