variable "project_name" {default = "glacier-infra"}
terraform {
  backend "s3" {
    bucket         = "glacier-infra"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "glacier-infra-lock"
  }
  required_version = ">= 1.0"
}
