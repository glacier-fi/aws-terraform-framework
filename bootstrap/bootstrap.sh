#!/usr/bin/env bash
export AWS_SECRET_ACCESS_ID=$AWS_SECRET_ACCESS_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export AWS_REGION=$AWS_DEFAULT_REGION
script_path="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $script_path
project_name=$(basename $(dirname $script_path))
stages=(entrypoints global devsecops)
for stage in "${stages[@]}"; do
  cat > "../${stage}/000_tf_conf.tf" <<EOF
variable "aws_account_id" {}
variable "region" {}

variable "tags" {
  type = object({
    env            = string
    product        = string
    country        = string
    source_of_truth = string
  })
}

provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket         = "${project_name}"
    region         = "${AWS_REGION}"
    encrypt        = true
    dynamodb_table = "${project_name}-lock"
  }
  required_version = ">= 1.0"
}
EOF
  if [[ $stage == "global" ]] ;then
    cat > "../global/terraform.tfvars" <<EOF
aws_account_id = 0000000000000
region         = "${AWS_REGION}"
tags = {
  env             = "$stage"
  product         = "lend"
  country         = "$stage"
  source_of_truth = "${project_name}"
}
EOF
  fi
  git add ../${stage}/*.tf
done
if  [ ! -f backend.tf ]; then
  terraform init
  terraform apply --auto-approve -var="project_name=$project_name"
  cat > backend.tf <<EOF
variable "project_name" {default = "${project_name}"}
terraform {
  backend "s3" {
    bucket         = "${project_name}"
    region         = "${AWS_REGION}"
    encrypt        = true
    dynamodb_table = "glacier-infra-lock"
  }
  required_version = ">= 1.0"
}
EOF
  sync
  git add backend.tf
  git commit -m "add bootstrap s3 backend configuration"
fi

terraform init -backend-config="bucket=${project_name}" \
  -backend-config="key=bootstrap/terraform.tfstate"
terraform apply -auto-approve -var="project_name=$project_name"
rm -rf .terraform.* terraform.*
echo "Copy those credentials to your gitlab"
